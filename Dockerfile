FROM ubuntu:22.04

ENV DEBIAN_FRONTEND="noninteractive"

# Make sure the image is updated, install some prerequisites,
# Download the latest version of Clang (official binary) for Ubuntu
# Extract the archive and add Clang to the PATH
RUN apt-get update \
  && apt-get install -y xz-utils make libc6-dev dpkg-dev curl p7zip-full unzip nano wget rsync libxml2 git lld-14 clang-14 clang-tools-14 \
  && rm -rf /var/lib/apt/lists/* \
  && ln /usr/bin/clang-14 /usr/bin/clang \
  && ln /usr/bin/clang++-14 /usr/bin/clang++ \
  && ln /usr/bin/clang-cpp-14 /usr/bin/clang-cpp \
  && ln /usr/bin/clang-cl-14 /usr/bin/clang-cl \
  && ln /usr/bin/llvm-ar-14 /usr/bin/llvm-ar \
  && ln /usr/bin/llvm-addr2line-14 /usr/bin/llvm-addr2line \
  && ln /usr/bin/llvm-lib-14 /usr/bin/llvm-lib \
  && ln /usr/bin/lld-14 /usr/bin/lld \
  && ln /usr/bin/lld-link-14 /usr/bin/lld-link \
  && ldconfig -v


# Start from a Bash prompt
CMD [ "/bin/bash" ]
